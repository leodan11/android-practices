package com.example.roomdemo

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.roomdemo.db.RoomAppDb
import com.example.roomdemo.db.UserDao
import com.example.roomdemo.db.UserEntity

class MainActivityViewModel : ViewModel() {

    private var allUsers: MutableLiveData<List<UserEntity>> = MutableLiveData<List<UserEntity>>()

    fun getAllUsersObservers(): MutableLiveData<List<UserEntity>>{
        return allUsers
    }

    private fun getAllUsers(application: Application) {
        val userDao: UserDao = RoomAppDb.getInstance(application).getUserDao()
        val list: List<UserEntity>? = userDao.getAllUserInfo()
        allUsers.postValue(list!!)
    }

    fun insertUserInfo(application: Application, contentValues: UserEntity) {
        val userDao: UserDao = RoomAppDb.getInstance(application).getUserDao()
        userDao.insertUser(contentValues)
        getAllUsers(application)
    }

    fun updateUserInfo(application: Application, contentValues: UserEntity) {
        val userDao: UserDao = RoomAppDb.getInstance(application).getUserDao()
        userDao.updateUser(contentValues)
        getAllUsers(application)
    }

    fun deleteUserInfo(application: Application, entity: UserEntity) {
        val userDao: UserDao = RoomAppDb.getInstance(application).getUserDao()
        userDao.deleteUser(entity)
        getAllUsers(application)
    }

}