package com.example.roomdemo

import com.example.roomdemo.db.UserEntity

interface CallbackItemListener {

    fun onDeleteUserClickListener(entity: UserEntity)
    fun onItemClickListener(entity: UserEntity)

}