package com.example.roomdemo.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [UserEntity::class], version = 1)
abstract class RoomAppDb: RoomDatabase() {

    abstract fun getUserDao(): UserDao

    companion object {
        private const val DATABASE_NAME = "user_database"
        @Volatile
        private var INSTANCE: RoomAppDb? = null

        fun getInstance(context: Context): RoomAppDb {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null){
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        RoomAppDb::class.java,
                        DATABASE_NAME
                    )
                        .allowMainThreadQueries()
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }

}