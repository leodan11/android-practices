package com.example.roomdemo

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.roomdemo.db.UserEntity

class MainActivity : AppCompatActivity(), CallbackItemListener {

    private lateinit var mainActivityViewModel: MainActivityViewModel
    private val userCustomAdapter: UserCustomAdapter = UserCustomAdapter(this@MainActivity)
    private lateinit var inputName: EditText
    private lateinit var inputEmail: EditText
    private lateinit var btnAction: Button

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        inputName = findViewById(R.id.editTextTextPersonName)
        inputEmail = findViewById(R.id.editTextTextPersonEmailAddress)
        btnAction = findViewById(R.id.buttonActions)
        val navHostContent: RecyclerView = findViewById(R.id.container_items)

        navHostContent.layoutManager = LinearLayoutManager(
            this@MainActivity,
            LinearLayoutManager.VERTICAL, //Mode Vertical
            false
        )
        navHostContent.setHasFixedSize(true)
        navHostContent.itemAnimator = DefaultItemAnimator()
        navHostContent.addItemDecoration(DividerItemDecoration(applicationContext, DividerItemDecoration.VERTICAL))
        navHostContent.adapter = userCustomAdapter

        mainActivityViewModel = ViewModelProvider(this)[MainActivityViewModel::class.java]
        mainActivityViewModel.getAllUsersObservers().observe(this, {
            userCustomAdapter.setListData(ArrayList(it))
            userCustomAdapter.notifyDataSetChanged()
        })

        btnAction.setOnClickListener {
            val name: String = inputName.text.toString()
            val email: String = inputEmail.text.toString()
            if (btnAction.text == "Guardar"){
                val entity: UserEntity = UserEntity(name_value = name, email_value = email)
                mainActivityViewModel.insertUserInfo(application, entity)
            }else{
                val entity: UserEntity = UserEntity(inputName.getTag(inputName.id).toString().toInt(), name, email)
                mainActivityViewModel.updateUserInfo(application, entity)
                btnAction.text = "Guardar"
            }
            inputName.text.clear()
            inputEmail.text.clear()
        }

    }

    override fun onDeleteUserClickListener(entity: UserEntity) {
        mainActivityViewModel.deleteUserInfo(application, entity)
    }

    override fun onItemClickListener(entity: UserEntity) {
        inputName.setText(entity.name_value)
        inputEmail.setText(entity.email_value)
        inputName.setTag(inputName.id, entity.id)
        btnAction.text = "Actualizar"
    }

}