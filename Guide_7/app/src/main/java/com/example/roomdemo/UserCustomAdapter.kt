package com.example.roomdemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.roomdemo.db.UserEntity

class UserCustomAdapter(private val listener: MainActivity): RecyclerView.Adapter<UserCustomAdapter.UserItem>() {

    private var items = ArrayList<UserEntity>()

    fun setListData(data: ArrayList<UserEntity>){
        this.items = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserItem {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        return UserItem(layoutInflater.inflate(R.layout.recycler_view_row, parent, false), listener)
    }

    override fun onBindViewHolder(holder: UserItem, position: Int) {
        holder.bind(items[position])
        holder.itemView.setOnClickListener {
            listener.onItemClickListener(items[position])
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class UserItem(view: View, private val listener: MainActivity) : RecyclerView.ViewHolder(view){

        var textViewName: TextView = view.findViewById(R.id.textViewUserName)
        var textViewEmail: TextView = view.findViewById(R.id.textViewUserEmail)
        var imageViewDeleteId: ImageView = view.findViewById(R.id.imageViewDeleteUserId)

        fun bind(entity: UserEntity) {
            textViewName.text = entity.name_value
            textViewEmail.text = entity.email_value
            imageViewDeleteId.setOnClickListener {
                listener.onDeleteUserClickListener(entity)
            }
        }

    }

}