package com.example.mypets.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiProperty {

    @GET("{id}/images")
    fun onGetAllImagesForBreed(@Path("id") idBreed: String): Call<Breed>

}