package com.example.mypets

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class BreedCustomAdapter(var mainActivity: MainActivity, var breeds: ArrayList<String>): RecyclerView.Adapter<BreedCustomAdapter.BreedItem>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BreedItem {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        return BreedItem(layoutInflater.inflate(R.layout.breed_item, parent, false))
    }

    override fun onBindViewHolder(holder: BreedItem, position: Int) {
        Glide.with(mainActivity)
            .load(breeds.get(position))
            .centerCrop()
            .placeholder(R.mipmap.ic_launcher)
            .into(holder.imageView)

        holder.itemView.setOnClickListener {
            Toast.makeText(mainActivity, "Elemento número: " + (position + 1).toString(), Toast.LENGTH_LONG).show()
        }

    }

    override fun getItemCount(): Int {
        return breeds.size
    }

    class BreedItem(view: View) : RecyclerView.ViewHolder(view){
        var imageView: ImageView = view.findViewById(R.id.imageViewBreed)
    }

}