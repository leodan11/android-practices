package com.example.mypets

import android.content.DialogInterface
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mypets.api.ApiClient
import com.example.mypets.api.Breed
import com.example.mypets.databinding.ActivityMainBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        val singleItems: Array<String> = arrayOf(
            "Affenpinscher",
            "Africano",
            "Airedale",
            "Akita",
            "Appenzeller",
            "Australian",
            "Buhund",
            "Chihuahua",
            "Frise",
            "Papillon",
            "Pinscher",
            "Pastor",
            "Tervuren ",
            "Weimaraner",
            "Whippet",
            "Perro Lobo"
        )

        val singleItemsOriginal: Array<String> = arrayOf(
            "affenpinscher",
            "african",
            "airedale",
            "akita",
            "appenzeller",
            "australian",
            "buhund",
            "chihuahua",
            "frise",
            "papillon",
            "pinscher",
            "sheepdog",
            "tervuren",
            "weimaraner",
            "whippet",
            "wolfhound"
        )

        var checkedItem: Int = 0

        binding.included.navHostContentMain.layoutManager = LinearLayoutManager( //Creamos forma de vizualizar elementos
            this@MainActivity,
            LinearLayoutManager.VERTICAL, //Mode Vertical
            false
        )
        binding.included.navHostContentMain.setHasFixedSize(true) //Optimización dependiendo del tamaño
        binding.included.navHostContentMain.itemAnimator = DefaultItemAnimator() //Animador por cada elemento existente

        binding.fab.setOnClickListener {
            MaterialAlertDialogBuilder(this@MainActivity)
                .setTitle("Selecione un elemento")
                .setCancelable(false)
                .setNeutralButton("Cancelar", object: DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        dialog?.dismiss()
                    }
                })
                .setPositiveButton("Aceptar", object: DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        val call: Call<Breed> = ApiClient.getClientApi.onGetAllImagesForBreed(singleItemsOriginal[checkedItem])
                        call.enqueue(object: Callback<Breed>{
                            override fun onResponse(call: Call<Breed>, response: Response<Breed>) {
                                if (response.isSuccessful){
                                    val breed: Breed? = response.body()
                                    if (breed?.status == "success"){
                                        val adapter = BreedCustomAdapter(this@MainActivity, breed.arrayListImages) //Creamos nuestro adaptador
                                        binding.included.navHostContentMain.adapter = adapter //Agregamos el adaptador a nuestra vista
                                        binding.included.navHostContentMain.visibility = View.VISIBLE
                                        binding.included.textViewEmpty.visibility = View.GONE
                                    }else{
                                        binding.included.navHostContentMain.visibility = View.GONE
                                        binding.included.textViewEmpty.visibility = View.VISIBLE
                                    }
                                }
                                dialog?.dismiss()
                            }

                            override fun onFailure(call: Call<Breed>, t: Throwable) {
                                call.cancel()
                                dialog?.dismiss()
                            }

                        })
                    }
                })
                .setSingleChoiceItems(singleItems, checkedItem, object: DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        checkedItem = which
                    }
                })
                .show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

}