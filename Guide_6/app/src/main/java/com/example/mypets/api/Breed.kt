package com.example.mypets.api

import com.google.gson.annotations.SerializedName

data class Breed(
    @SerializedName("message")
    var arrayListImages: ArrayList<String>,
    @SerializedName("status")
    var status: String
)
