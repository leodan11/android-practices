package com.example.actividades

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /**
         * Como nuestra aplicaion es bastante simple
         * No vamos a crear variables globales
         * todo lo tendremos aqui
         */

        //Obtengamos primeramente las referencias y las guardamos en variables
        val inputEditText: EditText = findViewById(R.id.editTextTextPersonName)
        val button: Button = findViewById(R.id.buttonSend)

        //Creamos el oyente, para cuando se presiones el boton enviar
        button.setOnClickListener {
            if (!TextUtils.isEmpty(inputEditText.text.toString())){
                val nombre: String = inputEditText.text.toString() //Obtengamos el nombre
                val intent: Intent = Intent(applicationContext, SecondActivity::class.java) //Creamos nuestra intencion
                intent.putExtra("name", nombre) //La informacion que compartiremos a la siguiente pantalla o actividad
                startActivity(intent) //Iniciamos la otra actividad
            }else{
                Toast.makeText(applicationContext, "Ingrese su nombre, por favor!", Toast.LENGTH_LONG).show()
            }
        }

    }

}