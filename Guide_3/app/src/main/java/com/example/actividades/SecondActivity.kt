package com.example.actividades

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        /**
         * Realizaremos las acciones solo en esta
         * funcion inical
         */
        //Obtengamos los identificadores de la vista y almacenarla en variables
        val title: TextView = findViewById(R.id.textViewHello)
        val button: Button = findViewById(R.id.buttonBack)

        //Ahora vamos a obtener el valor que nos emviaron desde la actividad anterior
        /**
         * El simbolo ? representa que puede ser un dato null
         * La opcion ?. para decir no es nulo y !! para determinar un
         * string no nulo
         */
        val bundle: Bundle? = intent.extras //obtenemos los datos extras obtenido.
        val name: String = bundle?.getString("name")!!
        title.text = getString(R.string.text_value_title) + " " + name

        //El escuchador para regresar a la actividad anterior
        button.setOnClickListener { finish() } //Basta con finalizar la actividad actual

    }

}