package com.example.shared

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var alertDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val textView: TextView = findViewById(R.id.textViewWelcome)
        val btnAction: Button = findViewById(R.id.buttonActionExit)

        textView.text = getString(R.string.text_value_welcome, PrefsLocal.getPreferenceFullName(applicationContext))

        alertDialog = ProgressDialog(this@MainActivity)
        alertDialog.setTitle("Finalizando la sesion")
        alertDialog.setMessage("Cerrando la Sesión. ¡Por Favor Espere!...")
        alertDialog.setCancelable(false)
        alertDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)

        btnAction.setOnClickListener {
            alertDialog.show()
            Handler(Looper.getMainLooper()).postDelayed(object: Runnable{
                override fun run() {
                    PrefsLocal.savePreferenceIsLogin(applicationContext, false)
                    alertDialog.dismiss()
                    startLoginActivity()
                }
            }, 1800)
        }

    }

    private fun startLoginActivity() {
        val intent: Intent = Intent(this@MainActivity, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

}