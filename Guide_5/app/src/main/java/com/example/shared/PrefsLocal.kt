package com.example.shared

import android.content.Context
import android.content.SharedPreferences

object PrefsLocal {

    private const val PREFS_NAME: String = "com.example.shared"
    private const val SHARED_FULL_NAME: String = "shared_name"
    private const val SHARED_USERNAME: String = "shared_username"
    private const val SHARED_PASSWORD: String = "shared_password"
    private const val SHARED_IS_LOGIN: String = "shared_login"

    fun savePreferenceFullName(context: Context, full_name: String){
        val preferences: SharedPreferences =
            context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        preferences.edit().putString(SHARED_FULL_NAME, full_name).apply()
    }

    fun getPreferenceFullName(context: Context): String? {
        val preferences: SharedPreferences =
            context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        return preferences.getString(
            SHARED_FULL_NAME,
            ""
        ) //if you have never been seed, this key returns an empty string
    }

    fun savePreferenceUserName(context: Context, username: String){
        val preferences: SharedPreferences =
            context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        preferences.edit().putString(SHARED_USERNAME, username).apply()
    }

    fun getPreferenceUserName(context: Context): String? {
        val preferences: SharedPreferences =
            context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        return preferences.getString(
            SHARED_USERNAME,
            ""
        ) //if you have never been seed, this key returns an empty string
    }

    fun savePreferencePassword(context: Context, password: String){
        val preferences: SharedPreferences =
            context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        preferences.edit().putString(SHARED_PASSWORD, password).apply()
    }

    fun getPreferencePassword(context: Context): String? {
        val preferences: SharedPreferences =
            context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        return preferences.getString(
            SHARED_PASSWORD,
            ""
        ) //if you have never been seed, this key returns an empty string
    }

    fun savePreferenceIsLogin(context: Context, isLogin: Boolean){
        val preferences: SharedPreferences =
            context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        preferences.edit().putBoolean(SHARED_IS_LOGIN, isLogin).apply()
    }

    fun getPreferenceIsLogin(context: Context): Boolean {
        val preferences: SharedPreferences =
            context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        return preferences.getBoolean(
            SHARED_IS_LOGIN,
            false
        ) //if you have never been seed, this key returns false
    }

}