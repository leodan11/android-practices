package com.example.shared

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.widget.Button
import android.widget.TextView
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class RegisterActivity : AppCompatActivity() {

    private lateinit var alertDialog: ProgressDialog
    private lateinit var nameLayout: TextInputLayout
    private lateinit var nameInput: TextInputEditText
    private lateinit var usernameLayout: TextInputLayout
    private lateinit var usernameInput: TextInputEditText
    private lateinit var passwordLayout: TextInputLayout
    private lateinit var passwordInput: TextInputEditText
    private lateinit var btnLogin: Button
    private lateinit var textAction: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        nameLayout = findViewById(R.id.textInputLayoutFullName)
        nameInput = findViewById(R.id.textInputEditFullName)
        usernameLayout = findViewById(R.id.textInputLayoutNewUsername)
        usernameInput = findViewById(R.id.textInputEditNewUsername)
        passwordLayout = findViewById(R.id.textInputLayoutNewPassword)
        passwordInput = findViewById(R.id.textInputEditNewPassword)
        btnLogin = findViewById(R.id.buttonContinue)
        textAction = findViewById(R.id.textViewActionBack)

        alertDialog = ProgressDialog(this@RegisterActivity)
        alertDialog.setTitle("Registrando nuevo usuario")
        alertDialog.setMessage("Registrando...\n Cargando Sesión. ¡Espere por favor!...")
        alertDialog.setCancelable(false)
        alertDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)

        btnLogin.setOnClickListener {
            alertDialog.show()
            Handler(Looper.getMainLooper()).postDelayed(object: Runnable{
                override fun run() {
                    if (validateOptions()){
                        alertDialog.dismiss()
                        startMainActivity()
                    }else{
                        alertDialog.dismiss()
                    }
                }
            }, 2500)
        }

        textAction.setOnClickListener {
            startLoginActivity()
        }

    }

    private fun startLoginActivity() {
        val intent: Intent = Intent(this@RegisterActivity, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    private fun validateOptions(): Boolean {

        if (TextUtils.isEmpty(nameInput.text.toString().trim())){
            nameLayout.error = "Nombre no puede estar vacio"
            nameLayout.isErrorEnabled = true
            return false
        }else if (nameInput.text.toString().length < 8){
            nameLayout.error = "Nombre demasiado pequeño"
            nameLayout.isErrorEnabled = true
            return false
        }else{
            PrefsLocal.savePreferenceFullName(applicationContext, nameInput.text.toString())
            nameLayout.isErrorEnabled = false
        }

        if (TextUtils.isEmpty(usernameInput.text.toString().trim())){
            usernameLayout.error = "Nombre de usuario no puede estar vacio"
            usernameLayout.isErrorEnabled = true
            return false
        }else if (usernameInput.text.toString().length < 8){
            usernameLayout.error = "Nombre de usuario demasiado pequeño"
            usernameLayout.isErrorEnabled = true
            return false
        }else{
            PrefsLocal.savePreferenceUserName(applicationContext, usernameInput.text.toString())
            usernameLayout.isErrorEnabled = false
        }

        if (TextUtils.isEmpty(passwordInput.text.toString().trim())){
            passwordLayout.error = "Contraseña no puede estar vacia"
            passwordLayout.isErrorEnabled = true
            passwordLayout.isHelperTextEnabled = false
            return false
        }else if (passwordInput.text.toString().length < 8){
            passwordLayout.error = "Contraseña demasiada pequeña"
            passwordLayout.isErrorEnabled = true
            passwordLayout.isHelperTextEnabled = false
            return false
        }else{
            PrefsLocal.savePreferencePassword(applicationContext, passwordInput.text.toString())
            passwordLayout.isErrorEnabled = false
            passwordLayout.isHelperTextEnabled = true
        }

        return true
    }

    private fun startMainActivity() {
        PrefsLocal.savePreferenceIsLogin(applicationContext, true)
        val intent: Intent = Intent(this@RegisterActivity, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

}