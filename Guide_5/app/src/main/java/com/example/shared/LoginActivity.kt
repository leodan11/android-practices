package com.example.shared

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class LoginActivity : AppCompatActivity() {

    private lateinit var alertDialog: ProgressDialog //Definimos las variables globales
    private lateinit var usernameLayout: TextInputLayout
    private lateinit var usernameInput: TextInputEditText
    private lateinit var passwordLayout: TextInputLayout
    private lateinit var passwordInput: TextInputEditText
    private lateinit var btnLogin: Button
    private lateinit var textAction: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (PrefsLocal.getPreferenceIsLogin(applicationContext)) startMainActivity() //Verificamos que exista sesión activa

        usernameLayout = findViewById(R.id.textInputLayoutUsername) //Obtenemos las referencias
        usernameInput = findViewById(R.id.textInputEditUsername)
        passwordLayout = findViewById(R.id.textInputLayoutPassword)
        passwordInput = findViewById(R.id.textInputEditPassword)
        btnLogin = findViewById(R.id.buttonLogin)
        textAction = findViewById(R.id.textViewNew)

        alertDialog = ProgressDialog(this@LoginActivity) //Creamos dialogo para mostrar
        alertDialog.setTitle("Iniciar Sesión")
        alertDialog.setMessage("Cargando Sesión. ¡Por Favor Espere!...")
        alertDialog.setCancelable(false)
        alertDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)

        btnLogin.setOnClickListener {
            alertDialog.show()
            Handler(Looper.getMainLooper()).postDelayed(object: Runnable{
                override fun run() {
                    if (validateOptions()){
                        alertDialog.dismiss()
                        startMainActivity()
                    }else{
                        alertDialog.dismiss()
                    }
                }
            }, 2500)
        }

        textAction.setOnClickListener {
            startRegisterActivity()
        }

    }

    private fun startRegisterActivity() {
        val intent: Intent = Intent(this@LoginActivity, RegisterActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    private fun validateOptions(): Boolean {
        if (TextUtils.isEmpty(usernameInput.text.toString().trim())){
            usernameLayout.error = "Ingrese un nombre de usuario"
            usernameLayout.isErrorEnabled = true
            return false
        }else {
            if (PrefsLocal.getPreferenceUserName(applicationContext) == ""){
                usernameLayout.error = "No Existe registro"
                usernameLayout.isErrorEnabled = true
                return false
            }else{
                if (PrefsLocal.getPreferenceUserName(applicationContext) != usernameInput.text.toString()){
                    usernameLayout.error = "Nombre de usuario incorrecto"
                    usernameLayout.isErrorEnabled = true
                    return false
                }else{
                    usernameLayout.isErrorEnabled = false
                }
            }
        }

        if (TextUtils.isEmpty(passwordInput.text.toString().trim())){
            usernameLayout.error = "Ingrese una contraseña"
            usernameLayout.isErrorEnabled = true
            return false
        }else {
            if (PrefsLocal.getPreferencePassword(applicationContext) == ""){
                usernameLayout.error = "No Existe registro"
                usernameLayout.isErrorEnabled = true
                return false
            }else{
                if (PrefsLocal.getPreferencePassword(applicationContext) != passwordInput.text.toString()){
                    usernameLayout.error = "Contraseña incorrecto"
                    usernameLayout.isErrorEnabled = true
                    return false
                }else{
                    usernameLayout.isErrorEnabled = false
                }
            }
        }
        return true
    }

    private fun startMainActivity() {
        PrefsLocal.savePreferenceIsLogin(applicationContext, true)
        val intent: Intent = Intent(this@LoginActivity, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }


}