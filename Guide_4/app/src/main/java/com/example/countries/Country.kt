package com.example.countries

data class Country(
    val name: String,
    val description: String,
    val resourceInt: Int
)