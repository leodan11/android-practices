package com.example.countries

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerView: RecyclerView = findViewById(R.id.container) //referencia de nuestra vista
        recyclerView.layoutManager = LinearLayoutManager( //Creamos forma de vizualizar elementos
            this@MainActivity,
            LinearLayoutManager.VERTICAL, //Mode Vertical
            false
        )
        recyclerView.setHasFixedSize(true) //Optimización dependiendo del tamaño
        val adapter = CountryCustomAdapter(this@MainActivity) //Creamos nuestro adaptador
        recyclerView.adapter = adapter //Agregamos el adaptador a nuestra vista
        recyclerView.itemAnimator = DefaultItemAnimator() //Animador por cada elemento existente
    }
}