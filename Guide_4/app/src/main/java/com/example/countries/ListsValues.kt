package com.example.countries

object ListsValues {

    val contentValues: MutableList<Country>
        get() {
            return mutableListOf(
                Country("Andorra",
                    "Capital: Andorra la Vella\n" +
                            "Superficie total: 468 km2\n" +
                            "Idioma oficial: catalán\n" +
                            "Moneda: euro",
                    R.drawable.ad),
                Country("Afganistán",
                    "Capital: Kabul\n" +
                            "Superficie total: 652,864 km2\n" +
                            "Idioma oficial: pashto y dari\n" +
                            "Moneda: afghani",
                    R.drawable.af),
                Country("Antigua y Barbuda",
                    "Capital: Saint John's\n" +
                            "Superficie total: 442 km2\n" +
                            "Idioma oficial: Inglés\n" +
                            "Moneda: Dólar del Caribe Oriental (XCD)",
                    R.drawable.ag),
                Country("Albania",
                    "Capital: Tirana\n" +
                            "Superficie total: 28,748 km2\n" +
                            "Idioma oficial: albanés\n" +
                            "Moneda: lek",
                    R.drawable.al),
                Country("República de Armenia",
                    "Capital: Yereván o Ereván\n" +
                            "Superficie total: 29,800 km2\n" +
                            "Idioma oficial: armenio\n " +
                            "Moneda: dram (AMD)",
                    R.drawable.am),
                Country("Angola",
                    "Capital: Luanda\n" +
                            "Superficie total: 1,246,700 (22º) km2\n" +
                            "Idioma oficial: Portugués\n" +
                            "Moneda: Kwanza (AOA)",
                    R.drawable.ao),
                Country("Argentina",
                    "Capital: Ciudad Autónoma de Buenos Aires\n" +
                            "Superficie total: 2,780,400 km2\n" +
                            "Idioma oficial: español\n" +
                            "Moneda: Peso argentino (ARS)",
                    R.drawable.ar),
                Country("Austria",
                    "Capital: Viena\n" +
                            "Superficie total: 83,871 (112º) km2\n" +
                            "Idioma oficial: Alemán\n" +
                            "Moneda: Euro (antes chelín austriaco) (EUR)",
                    R.drawable.at),
                Country("Australia",
                    "Capital: Canberra\n" +
                            "Superficie total: 7,682,395 km2\n" +
                            "Idioma oficial: inglés\n" +
                            "Moneda: Dólar australiano",
                    R.drawable.au),
                Country("Aruba",
                    "Capital: Oranjestad\n" +
                            "Superficie total: 180 km2\n" +
                            "Idioma oficial: papiamento, neerlandés\n" +
                            "Moneda: florín arubeño",
                    R.drawable.aw),
                Country("Azerbaiyán",
                    "Capital: Bakú\n" +
                            "Superficie total: 86,600 km2\n" +
                            "Idioma oficial: azerbaiyano\n" +
                            "Moneda: manat (AZM)",
                    R.drawable.az)
            )
        }
}