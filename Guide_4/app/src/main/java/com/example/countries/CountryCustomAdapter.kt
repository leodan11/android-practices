package com.example.countries

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class CountryCustomAdapter(private val context: Context) :
    RecyclerView.Adapter<CountryCustomAdapter.CountryItem>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryItem {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        return CountryItem(layoutInflater.inflate(R.layout.country_item, parent, false))
    }

    override fun onBindViewHolder(holder: CountryItem, position: Int) {
        holder.imageView.setImageResource(ListsValues.contentValues[position].resourceInt)
        holder.textView.text = ListsValues.contentValues[position].name
        holder.linearLayout.setOnClickListener {
            MaterialAlertDialogBuilder(context)
                .setIcon(ListsValues.contentValues[position].resourceInt)
                .setTitle(ListsValues.contentValues[position].name)
                .setMessage(ListsValues.contentValues[position].description)
                .setCancelable(false)
                .setPositiveButton(R.string.action_positive_action) { dialog, _ ->
                    dialog.dismiss()
                }.create().show()
        }
    }

    override fun getItemCount(): Int {
        return ListsValues.contentValues.size
    }

    class CountryItem(view: View) : RecyclerView.ViewHolder(view){
        var imageView: ImageView = view.findViewById(R.id.imageViewIcon)
        var  textView: TextView = view.findViewById(R.id.textViewNameCountry)
        var  linearLayout: LinearLayout = view.findViewById(R.id.container_more)

    }

}