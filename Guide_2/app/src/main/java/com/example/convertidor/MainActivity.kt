package com.example.convertidor

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.*

class MainActivity : AppCompatActivity() {

    /**
     * Variables Globales
     * Para hacer referencia a cada uno de los elementos de la vista
     */
    private lateinit var inputQuery: EditText //Nuestra entrada de datos
    private lateinit var radioButtonC: RadioButton //Si vamos a convertir de Celsius a Fahrenheit
    private lateinit var radioButtonF: RadioButton //Si vamos a converir de Fahrenheit a Celsius
    private lateinit var textResult: TextView //Para Mostrar el resultado
    private lateinit var btnAction: Button //Nuestro botón que inicia la conversión

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /**
         * Obtenemos las referencias
         * de cada una de las vista y las almacenamos en las variables
         */
        inputQuery = findViewById(R.id.editTextQuery)
        radioButtonC = findViewById(R.id.radioButtonCtoF)
        radioButtonF = findViewById(R.id.radioButtonFtoC)
        textResult = findViewById(R.id.textViewResult)
        btnAction = findViewById(R.id.buttonActionConverter)

        /**
         * Oyente al hacer click a nuestro botón
         */
        btnAction.setOnClickListener {
            if (!TextUtils.isEmpty(inputQuery.text.toString())){ //Comprobamos que tengamos un valor para convertir
                if (radioButtonC.isChecked)
                    onConverterCtoF()
                else if (radioButtonF.isChecked)
                    onConverterFtoC()
            }else{//Sino lo hay mostramos un pequeño mensaje
                Toast.makeText(applicationContext, "Debe ingresar un valor", Toast.LENGTH_LONG).show()
                textResult.visibility = View.GONE //Ocultar el texto donde mostramos el resultado
            }
        }

    }

    /**
     * Función que permite convertir de Fahrenheit a Celsius
     */
    private fun onConverterFtoC() {
        val valueInput: Double = inputQuery.text.toString().toDouble() //Obtenemos el dato
        val valueFirst: Double = valueInput - 32 //Realizamos la ecuación
        val valueEnd: Double = (5.0 / 9.0)
        val result: Double = valueFirst * valueEnd
        textResult.text = "Los " + valueInput + "ºF equivalen a " + result + "ºC." //Mostramos resultado
        textResult.visibility = View.VISIBLE //Volver visible el texto
    }

    /**
     * Función que permite convertir de Celsius a Fahrenheit
     */
    private fun onConverterCtoF() {
        val valueInput: Double = inputQuery.text.toString().toDouble() //Obtenemos el valor
        val valueFirst: Double = (9.0 / 5.0) //Realizamos la ecuación
        val valueEnd: Double = valueInput * valueFirst
        val result: Double = valueEnd + 32.0
        textResult.text = "Los " + valueInput + "ºC equivalen a " + result + "ºF." //Mostramos resultado
        textResult.visibility = View.VISIBLE //Volver visible el texto
    }

}